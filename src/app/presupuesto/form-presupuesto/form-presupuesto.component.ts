import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IPresupuesto } from 'src/app/service/interface';

@Component({
  selector: 'app-form-presupuesto',
  templateUrl: './form-presupuesto.component.html',
  styleUrls: ['./form-presupuesto.component.scss']
})
export class FormPresupuestoComponent implements OnInit {

  @Input() data: IPresupuesto = '';
  @Output() valid: EventEmitter<boolean> = new EventEmitter();
  @Output() validatedData: EventEmitter<IPresupuesto> = new EventEmitter();

  validateForm: FormGroup;



  constructor(private fb: FormBuilder) {
    this.validateForm = this.fb.group({})
  }

  ngOnInit(): void {
    this.crearFormulario();
    this.valid.emit(this.validateForm.valid);
    this.validateForm.valueChanges.subscribe((value) => {
      this.valid.emit(this.validateForm.valid);
      /* this.validateForm.valid
        ? this.validatedData.emit(value)
        : this.validatedData.emit(null); */
    });
  }

  crearFormulario(): void {
    this.validateForm = this.fb.group({
      pasos:
        this.fb.array(
          [
            this.fb.group({
              /* nombrePresupuesto: [this.data ? this.data. : '', [Validators.required]], */
              primerCampo: ['', [Validators.required]],
              segundoCampo: ['', [Validators.required]],
            }),
            this.fb.group({
              /* nombrePresupuesto: [this.data ? this.data. : '', [Validators.required]], */
              tercerCampo: ['', [Validators.required]],
              cuartoCampo: ['', [Validators.required]],
            }),
            this.fb.group({
              /* nombrePresupuesto: [this.data ? this.data. : '', [Validators.required]], */
              quintoCampo: ['', [Validators.required]],
              sextoCampo: ['', [Validators.required]],
            }),
            this.fb.group({
              /* nombrePresupuesto: [this.data ? this.data. : '', [Validators.required]], */
              quintoCampo: ['', [Validators.required]],
              sextoCampo: ['', [Validators.required]],
            }),
          ]

        )
    })



  }

  current = 0;
  index = 'primer-paso';

  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'primer-paso';
        break;
      }
      case 1: {
        this.index = 'segundo-paso';
        break;
      }
      case 2: {
        this.index = 'tercer-paso';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }

}
