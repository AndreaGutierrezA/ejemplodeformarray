import { Component, OnInit } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: ['./presupuesto.component.scss']
})
export class PresupuestoComponent implements OnInit {
  nombrePeriodo: string = '';
  año: string = '2022';
  // modal
  isVisible = false;
  valid = false;
  validatedData: any;
  data: any; 
 
  
  incluirBorrados = false;
 
  confirmModal?: NzModalRef;
 
  // tabla
  searchValue = '';
  visible = false;
 
  /* empresarios: IEmpresario[]; */
 
  /* empresariosData: IEmpresario[]; */
 
  constructor(
    private modal: NzModalService,
  ) { }
 
  ngOnInit(): void {
    this.nombrePeriodo = "Crear presupuesto - " + this.año
  }
  // modal
 
  showModal(): void {
    this.isVisible = true;
  }
 
  isValid(event: any): void {
    this.valid = event;
  }
 
  getData(event: any): void {
    this.validatedData = event;
  }
 
  handleOk(): void {
    this.isVisible = false;
    this.valid = false;
 
    
  }
 
  handleCancel(): void {
    this.isVisible = false;
    this.valid = false;
  }
 
  onCreate(): void {
    this.showModal();
  }
  onEdit(item: any): void {
    this.showModal();
  }
 
  /* reset(): void {
    this.searchValue = '';
    this.search();
  } */
 
}
